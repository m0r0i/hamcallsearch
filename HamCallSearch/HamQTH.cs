﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace HamCallSearch
{
    static class HamQTH
    {
        // private variables:
        private static string url = "";
        private static string ausgabe = "";

        // Properties:
        internal static string CallsignDetails { get; set; }
        internal static string StatusLabel { get; set; }
        internal static string PicBox { get; set; }
        internal static ArrayList infolist = new ArrayList();


        /// <summary>
        /// Here we fetch the Session ID from HamQTH 
        /// </summary>
        /// <param name="OwnCallSign">Callsign of the user</param>
        /// <param name="OwnPasssword">PW of the user</param>
        /// <param name="session_ID">the required session ID we need to get data</param>
        /// <returns></returns>
        public static string GetSessionID(string OwnCallSign, string OwnPasssword, out string session_ID)
        {

            string url = "https://www.hamqth.com/xml.php?u=" + OwnCallSign + "&p=" + OwnPasssword;

            bool error = false;

            session_ID = "";

            using (XmlTextReader reader = new XmlTextReader(url))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element: // The node is an element.

                            if (reader.Name == "HamQTH" || reader.Name == "version" || reader.Name == "xmlns" || reader.Name == "session")
                            {
                                CallsignDetails += " ";
                            }
                            else
                            {
                                if (reader.Name == "error")
                                {
                                    StatusLabel = "ERROR getting SID.";
                                    error = true;
                                }
                                else if (reader.Name == "session_id")
                                {
                                    ausgabe = "SID    : ";
                                    error = false;
                                }
                                //LblCallsignDetails.Text += "\n" + ausgabe;
                            }

                            break;
                        case XmlNodeType.Text: //Display the text in each element.
                            if (error)
                            {
                                session_ID = "ERROR";
                                ausgabe += reader.Value;
                                MessageBox.Show(ausgabe, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                break;
                            }

                            session_ID = reader.Value;
                            ausgabe = "";
                            break;




                    }
                }
            }

            ausgabe = "";
            return session_ID;
        }


        /// <summary>
        /// DXCC (HamQTH quickinfo) - search
        /// </summary>
        /// <param name="s_call">The call to search for</param>
        public static void DXCC_Search(string s_call)
        {
            url = "https://www.hamqth.com/dxcc.php?callsign=" + s_call;
            
            string lstausgabe;
            string eingabe;
            string[] teil;


            using (XmlTextReader reader = new XmlTextReader(url))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element: // The node is an element.

                            if (reader.Name == "HamQTH" || reader.Name == "version" || reader.Name == "xmlns" || reader.Name == "dxcc")
                            {
                                ausgabe = "";
                            }
                            else
                            {
                                if (reader.Name == "error")
                                {
                                    // Call not found....
                                    ausgabe = "";
                                    StatusLabel = "ERROR! Call not found.";
                                    infolist.Add("No results...");
                                    break;
                                }
                                else if (reader.Name == "callsign")
                                    ausgabe = "Callsign : ";
                                else if (reader.Name == "name")
                                    ausgabe = "Country  : ";
                                else if (reader.Name == "details")
                                    ausgabe = "Details  : ";
                                else if (reader.Name == "continent")
                                    ausgabe = "Continent: ";
                                else if (reader.Name == "utc")
                                    ausgabe = "UTC      : ";
                                else if (reader.Name == "waz")
                                    ausgabe = "WAZ      : ";
                                else if (reader.Name == "itu")
                                    ausgabe = "ITU      : ";
                                else if (reader.Name == "lat")
                                    ausgabe = "Latitude : ";
                                else if (reader.Name == "lng")
                                    ausgabe = "Longitude: ";
                                else if (reader.Name == "adif")
                                    ausgabe = "ADIF     : ";

                            }

                            while (reader.MoveToNextAttribute()) // Read the attributes.


                                if (reader.LocalName == "HamQTH" || reader.LocalName == "version" || reader.LocalName == "xmlns" || reader.LocalName == "dxcc")
                                {
                                    ausgabe = "";
                                }
                                else
                                {
                                    if (reader.LocalName == "callsign")
                                        ausgabe = "Callsign : ";
                                    else if (reader.LocalName == "name")
                                        ausgabe = "Country  : ";
                                    else if (reader.LocalName == "details")
                                        ausgabe = "Details  : ";
                                    else if (reader.LocalName == "continent")
                                        ausgabe = "Continent: ";
                                    else if (reader.LocalName == "utc")
                                        ausgabe = "UTC      : ";
                                    else if (reader.LocalName == "waz")
                                        ausgabe = "WAZ      : ";
                                    else if (reader.LocalName == "itu")
                                        ausgabe = "ITU      : ";
                                    else if (reader.LocalName == "lat")
                                        ausgabe = "Latitude : ";
                                    else if (reader.LocalName == "lng")
                                        ausgabe = "Longitude: ";
                                    else if (reader.LocalName == "adif")
                                        ausgabe = "ADIF     : ";

                                    infolist.Add(ausgabe);
                                    ausgabe = "";
                                }
                            break;

                        case XmlNodeType.Text: //Display the text in each element.

                            eingabe = reader.Value;
                            teil = eingabe.Split(',');

                            for (int i = 0; i < teil.Length; i++)
                            {
                                if (i == 0)
                                {
                                    lstausgabe = ausgabe + " " + teil[i];
                                    infolist.Add(lstausgabe);
                                }
                                else
                                {
                                    lstausgabe = ausgabe + " " + teil[i];
                                    infolist.Add(lstausgabe);
                                }
                            }
                            break;

                    }
                }
            }
        }

        /// <summary>
        /// The search for details of the call
        /// </summary>
        /// <param name="sessionID"></param>
        /// <param name="s_call">Call to search for</param>
        /// <param name="owncall">own call</param>
        /// <param name="ownpw">own pw</param>
        public static void CallSearch(string sessionID, string s_call, string owncall, string ownpw)
        {        
            bool picurl = false;
            string lstausgabe;
            string eingabe;
            string[] teil;

            // We need the Call + PW of the OM, otherwise we don't get a session ID.
            if (!string.IsNullOrEmpty(owncall) && !string.IsNullOrEmpty(ownpw))
            {

                // ok, seems we have Call + Password, let's retreive the Session ID if we don't already have one:
                while (string.IsNullOrEmpty(sessionID) || sessionID == "ERROR")
                {
                    GetSessionID(owncall, ownpw, out sessionID);
                }


                // If we have a valid SID, let's go
                url = "https://www.hamqth.com/xml.php?id=" + sessionID + "&callsign=" + s_call + "&prg=HamCallSearch";

                infolist.Clear();



                using (XmlTextReader reader = new XmlTextReader(url))
                {
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element: // The node is an element.

                                if (reader.Name == "HamQTH" || reader.Name == "version" || reader.Name == "xmlns" || reader.Name == "dxcc")
                                {
                                    ausgabe = "skip";

                                }
                                else
                                {
                                    if (reader.Name == "error")
                                    {
                                        // Call not found....
                                        ausgabe = "";
                                        StatusLabel = "ERROR! Call not found.";
                                        infolist.Add("No results...");
                                        break;
                                    }

                                    else if (reader.Name == "callsign")
                                        ausgabe = "Callsign  : ";
                                    else if (reader.Name == "nick")
                                        ausgabe = "Nick      : ";
                                    else if (reader.Name == "qth")
                                        ausgabe = "QTH       : ";
                                    else if (reader.Name == "country")
                                        ausgabe = "Country   : ";
                                    else if (reader.Name == "adif")
                                        ausgabe = "ADIF      : ";
                                    else if (reader.Name == "itu")
                                        ausgabe = "ITU       : ";
                                    else if (reader.Name == "cq")
                                        ausgabe = "WAZ/CQ    : ";
                                    else if (reader.Name == "grid")
                                        ausgabe = "Locator   : ";
                                    else if (reader.Name == "adr_name")
                                        ausgabe = "Name      : ";
                                    else if (reader.Name == "adr_street1")
                                        ausgabe = "Street    : ";
                                    else if (reader.Name == "adr_street2")
                                        ausgabe = "Street    : ";
                                    else if (reader.Name == "adr_street3")
                                        ausgabe = "Street    : ";
                                    else if (reader.Name == "adr_city")
                                        ausgabe = "City      : ";
                                    else if (reader.Name == "adr_zip")
                                        ausgabe = "PostCode  : ";
                                    else if (reader.Name == "adr_country")
                                        ausgabe = "Country   : ";
                                    else if (reader.Name == "adr_adif")
                                        ausgabe = "Adr. ADIF : ";
                                    else if (reader.Name == "district")
                                        ausgabe = "District  : ";
                                    else if (reader.Name == "us_state")
                                        ausgabe = "US State  : ";
                                    else if (reader.Name == "us_county")
                                        ausgabe = "US County : ";
                                    else if (reader.Name == "oblast")
                                        ausgabe = "Oblast    : ";
                                    else if (reader.Name == "dok")
                                        ausgabe = "DOK       : ";
                                    else if (reader.Name == "iota")
                                        ausgabe = "IOTA      : ";
                                    else if (reader.Name == "qsl_via")
                                        ausgabe = "QSL via   : ";
                                    else if (reader.Name == "lotw")
                                        ausgabe = "Uses LOTW : ";
                                    else if (reader.Name == "qsl")
                                        ausgabe = "QSL       : ";
                                    else if (reader.Name == "qsl_direct")
                                        ausgabe = "QSL direct: ";
                                    else if (reader.Name == "eqsl")
                                        ausgabe = "EQSL      : ";
                                    else if (reader.Name == "email")
                                        ausgabe = "Email     : ";
                                    else if (reader.Name == "jabber")
                                        ausgabe = "Jabber    : ";
                                    else if (reader.Name == "skype")
                                        ausgabe = "Skype     : ";
                                    else if (reader.Name == "birth_year")
                                        ausgabe = "Born in   : ";
                                    else if (reader.Name == "lic_year")
                                        ausgabe = "Licensed  : ";
                                    else if (reader.Name == "web")
                                        ausgabe = "Homepage  : ";
                                    else if (reader.Name == "latitude")
                                        ausgabe = "Latitude  : ";
                                    else if (reader.Name == "longitude")
                                        ausgabe = "Longitude : ";
                                    else if (reader.Name == "continent")
                                        ausgabe = "Continent : ";
                                    else if (reader.Name == "utc_offset")
                                        ausgabe = "UTC offset: ";
                                    else if (reader.Name == "picture")
                                        picurl = true;

                                    else
                                        ausgabe = "";

                                }

                                break;

                            case XmlNodeType.Text: //Display the text in each element.


                                // Do we have a field we don't want, then skip it
                                // Convert lat/long to double and display correctly 


                                double latlon;
                                eingabe = reader.Value;
                                teil = eingabe.Split(',');


                                if (picurl == true)
                                {
                                    PicBox = reader.Value;
                                    break;
                                }

                                else if (reader.Name == "longitude")
                                {
                                    latlon = Convert.ToDouble(reader.Value);
                                    infolist.Add(latlon);
                                    break;
                                }

                                else if (reader.Name == "latitude")
                                {
                                    latlon = Convert.ToDouble(reader.Value);
                                    infolist.Add(latlon);
                                    break;
                                }

                                else if (ausgabe == "skip" || ausgabe == "")
                                    break;


                                // Here we go: display the information and break the line if necessary
                                for (int i = 0; i < teil.Length; i++)
                                    if (i == 0)
                                    {
                                        lstausgabe = ausgabe + " " + teil[i];
                                        infolist.Add(lstausgabe);
                                    }
                                    else
                                    {
                                        lstausgabe = "\n        " + teil[i];
                                        infolist.Add(lstausgabe);
                                    }

                                // Mission accomplished :)
                                StatusLabel = "Status: Ready.";
                                break;

                        }
                    }
                }
            }

            // Hey, no own call and password found.... that won't work!
            else
            {
                MessageBox.Show("Please enter your callsign and password for HamQTH if you want to use the detailed call search", "Missing infos...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                StatusLabel = "No own call and/or password!";
            }


        }
    }
}

﻿namespace HamCallSearch
{
	partial class Form1
	{
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		

		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		/// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Vom Windows Form-Designer generierter Code

		/// <summary>
		/// Erforderliche Methode für die Designerunterstützung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.TxtEnterCallsign = new System.Windows.Forms.TextBox();
            this.CmdDXCCSearch = new System.Windows.Forms.Button();
            this.LblEnterCallsign = new System.Windows.Forms.Label();
            this.LblCallsignDetails = new System.Windows.Forms.Label();
            this.CmdCallSearch = new System.Windows.Forms.Button();
            this.TxtMyCall = new System.Windows.Forms.TextBox();
            this.TxtMyPW = new System.Windows.Forms.TextBox();
            this.LblMyCall = new System.Windows.Forms.Label();
            this.LblMyPW = new System.Windows.Forms.Label();
            this.LstDetails = new System.Windows.Forms.ListBox();
            this.PicBox = new System.Windows.Forms.PictureBox();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.StaLabelZeit = new System.Windows.Forms.ToolStripStatusLabel();
            this.StaLabelVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.StaLabelStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PicBox)).BeginInit();
            this.StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // TxtEnterCallsign
            // 
            this.TxtEnterCallsign.Location = new System.Drawing.Point(16, 39);
            this.TxtEnterCallsign.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtEnterCallsign.Name = "TxtEnterCallsign";
            this.TxtEnterCallsign.Size = new System.Drawing.Size(187, 22);
            this.TxtEnterCallsign.TabIndex = 0;
            this.TxtEnterCallsign.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Eingabe_KeyDown);
            // 
            // CmdDXCCSearch
            // 
            this.CmdDXCCSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdDXCCSearch.Location = new System.Drawing.Point(16, 71);
            this.CmdDXCCSearch.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CmdDXCCSearch.Name = "CmdDXCCSearch";
            this.CmdDXCCSearch.Size = new System.Drawing.Size(188, 53);
            this.CmdDXCCSearch.TabIndex = 1;
            this.CmdDXCCSearch.Text = "DXCC Search";
            this.CmdDXCCSearch.UseVisualStyleBackColor = true;
            this.CmdDXCCSearch.Click += new System.EventHandler(this.CmdDXCCSearch_Click);
            // 
            // LblEnterCallsign
            // 
            this.LblEnterCallsign.AutoSize = true;
            this.LblEnterCallsign.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEnterCallsign.Location = new System.Drawing.Point(16, 20);
            this.LblEnterCallsign.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblEnterCallsign.Name = "LblEnterCallsign";
            this.LblEnterCallsign.Size = new System.Drawing.Size(149, 17);
            this.LblEnterCallsign.TabIndex = 2;
            this.LblEnterCallsign.Text = "Callsign to look for:";
            // 
            // LblCallsignDetails
            // 
            this.LblCallsignDetails.AutoSize = true;
            this.LblCallsignDetails.Location = new System.Drawing.Point(16, 119);
            this.LblCallsignDetails.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblCallsignDetails.Name = "LblCallsignDetails";
            this.LblCallsignDetails.Size = new System.Drawing.Size(0, 17);
            this.LblCallsignDetails.TabIndex = 3;
            // 
            // CmdCallSearch
            // 
            this.CmdCallSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdCallSearch.Location = new System.Drawing.Point(213, 71);
            this.CmdCallSearch.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CmdCallSearch.Name = "CmdCallSearch";
            this.CmdCallSearch.Size = new System.Drawing.Size(188, 53);
            this.CmdCallSearch.TabIndex = 4;
            this.CmdCallSearch.Text = "Call Search";
            this.CmdCallSearch.UseVisualStyleBackColor = true;
            this.CmdCallSearch.Click += new System.EventHandler(this.CmdCallSearch_Click);
            // 
            // TxtMyCall
            // 
            this.TxtMyCall.Location = new System.Drawing.Point(452, 55);
            this.TxtMyCall.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtMyCall.MaxLength = 6;
            this.TxtMyCall.Name = "TxtMyCall";
            this.TxtMyCall.Size = new System.Drawing.Size(164, 22);
            this.TxtMyCall.TabIndex = 5;
            this.TxtMyCall.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Call_PressedEnter);
            // 
            // TxtMyPW
            // 
            this.TxtMyPW.Location = new System.Drawing.Point(452, 98);
            this.TxtMyPW.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtMyPW.Name = "TxtMyPW";
            this.TxtMyPW.Size = new System.Drawing.Size(164, 22);
            this.TxtMyPW.TabIndex = 6;
            this.TxtMyPW.UseSystemPasswordChar = true;
            this.TxtMyPW.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PW_PressedEnter);
            // 
            // LblMyCall
            // 
            this.LblMyCall.AutoSize = true;
            this.LblMyCall.Location = new System.Drawing.Point(452, 38);
            this.LblMyCall.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblMyCall.Name = "LblMyCall";
            this.LblMyCall.Size = new System.Drawing.Size(79, 17);
            this.LblMyCall.TabIndex = 7;
            this.LblMyCall.Text = "My Callsign";
            // 
            // LblMyPW
            // 
            this.LblMyPW.AutoSize = true;
            this.LblMyPW.Location = new System.Drawing.Point(452, 81);
            this.LblMyPW.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblMyPW.Name = "LblMyPW";
            this.LblMyPW.Size = new System.Drawing.Size(95, 17);
            this.LblMyPW.TabIndex = 8;
            this.LblMyPW.Text = "My Password:";
            // 
            // LstDetails
            // 
            this.LstDetails.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LstDetails.FormattingEnabled = true;
            this.LstDetails.ItemHeight = 18;
            this.LstDetails.Location = new System.Drawing.Point(16, 139);
            this.LstDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LstDetails.Name = "LstDetails";
            this.LstDetails.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.LstDetails.Size = new System.Drawing.Size(596, 220);
            this.LstDetails.TabIndex = 9;
            // 
            // PicBox
            // 
            this.PicBox.Location = new System.Drawing.Point(16, 368);
            this.PicBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PicBox.Name = "PicBox";
            this.PicBox.Size = new System.Drawing.Size(597, 231);
            this.PicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicBox.TabIndex = 10;
            this.PicBox.TabStop = false;
            // 
            // StatusStrip
            // 
            this.StatusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StaLabelZeit,
            this.StaLabelVersion,
            this.StaLabelStatus});
            this.StatusStrip.Location = new System.Drawing.Point(0, 603);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.StatusStrip.Size = new System.Drawing.Size(637, 26);
            this.StatusStrip.TabIndex = 11;
            this.StatusStrip.Text = "statusStrip1";
            // 
            // StaLabelZeit
            // 
            this.StaLabelZeit.Name = "StaLabelZeit";
            this.StaLabelZeit.Size = new System.Drawing.Size(74, 20);
            this.StaLabelZeit.Text = "DateTime";
            // 
            // StaLabelVersion
            // 
            this.StaLabelVersion.Name = "StaLabelVersion";
            this.StaLabelVersion.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.StaLabelVersion.Size = new System.Drawing.Size(72, 20);
            this.StaLabelVersion.Text = "Version";
            // 
            // StaLabelStatus
            // 
            this.StaLabelStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.StaLabelStatus.Name = "StaLabelStatus";
            this.StaLabelStatus.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.StaLabelStatus.Size = new System.Drawing.Size(71, 20);
            this.StaLabelStatus.Text = "Status: ";
            this.StaLabelStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(448, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(161, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Enter for call search:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 629);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.PicBox);
            this.Controls.Add(this.LstDetails);
            this.Controls.Add(this.LblMyPW);
            this.Controls.Add(this.LblMyCall);
            this.Controls.Add(this.TxtMyPW);
            this.Controls.Add(this.TxtMyCall);
            this.Controls.Add(this.CmdCallSearch);
            this.Controls.Add(this.LblCallsignDetails);
            this.Controls.Add(this.LblEnterCallsign);
            this.Controls.Add(this.CmdDXCCSearch);
            this.Controls.Add(this.TxtEnterCallsign);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "HamCallSearch         (c) 2018, 2019 DO7WHP";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PicBox)).EndInit();
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox TxtEnterCallsign;
		private System.Windows.Forms.Button CmdDXCCSearch;
		private System.Windows.Forms.Label LblEnterCallsign;
		private System.Windows.Forms.Label LblCallsignDetails;
		private System.Windows.Forms.Button CmdCallSearch;
		private System.Windows.Forms.TextBox TxtMyCall;
		private System.Windows.Forms.TextBox TxtMyPW;
		private System.Windows.Forms.Label LblMyCall;
		private System.Windows.Forms.Label LblMyPW;
		private System.Windows.Forms.ListBox LstDetails;
		private System.Windows.Forms.PictureBox PicBox;
		private System.Windows.Forms.StatusStrip StatusStrip;
		private System.Windows.Forms.ToolStripStatusLabel StaLabelZeit;
		private System.Windows.Forms.ToolStripStatusLabel StaLabelVersion;
		private System.Windows.Forms.ToolStripStatusLabel StaLabelStatus;
		private System.Windows.Forms.Label label2;
	}
}


﻿using System;
using System.Xml;
using System.Windows.Forms;


namespace HamCallSearch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            MessageBox.Show("If you only need short DXCC info enter Callsign to search and hit 'Quick Search'.\n\n For details of the call enter your HamQTH credentials and click 'Call Search'.", "USAGE INFO HamCallSearch", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public string ProgrammName = "HamCallSearch";
        public string ProgrammVersion = Application.ProductVersion.ToString();
        private readonly string SessionID = "";


        // ***  Get the short DXCC-Infos from HamQTH without details *** 
        private void CmdDXCCSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtEnterCallsign.Text))
            {
                MessageBox.Show("Please enter a call to look for!", "You forgot something...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            PicBox.Image = null;
            LstDetails.Items.Clear();
            StaLabelStatus.Text = "Loading.  (Be patient...!)";

            HamQTH.DXCC_Search(TxtEnterCallsign.Text);

            StaLabelStatus.Text = HamQTH.StatusLabel;

            foreach (var item in HamQTH.infolist)
            {
                LstDetails.Items.Add(item as string);
            }

            StaLabelStatus.Text = "Status: Ready.";
        }


        // Hitting Enter in the Callsign-to-search-field
        private void Eingabe_KeyDown(object sender, KeyEventArgs e)
        {
            StaLabelStatus.Text = "Loading...";

            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                if (TxtMyCall.Text != "" && TxtMyPW.Text != "")
                {
                    StaLabelStatus.Text = "Status: Get SessionID...";
                    CmdCallSearch_Click(sender, e);
                }
                else
                {
                    StaLabelStatus.Text = "Status: Get SessionID...";
                    CmdDXCCSearch_Click(sender, e);
                }


            }


        }


        // *** Search for detailed information ***
        private void CmdCallSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtEnterCallsign.Text))
            {
                MessageBox.Show("Please enter a call to look for!", "You forgot something...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (string.IsNullOrEmpty(TxtMyCall.Text) && string.IsNullOrEmpty(TxtMyPW.Text))
            {
                MessageBox.Show("Please enter your HamQTH-Callsign and Password!", "Can't connect to HamQTH.com", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            StaLabelStatus.Text = "Status: Loading. (Be patient...!)";
            LstDetails.Items.Clear();
            HamQTH.CallSearch(SessionID, TxtEnterCallsign.Text, TxtMyCall.Text, TxtMyPW.Text);
            PicBox.Load(HamQTH.PicBox);
            LblCallsignDetails.Text = HamQTH.CallsignDetails;
            StaLabelStatus.Text = HamQTH.StatusLabel;

            foreach (var item in HamQTH.infolist)
            {
                LstDetails.Items.Add(item as string);
            }

            StaLabelStatus.Text = HamQTH.StatusLabel;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Put initial values to the status bar
            StaLabelZeit.Text = DateTime.Today.ToShortDateString();
            StaLabelVersion.Text = "Version " + ProgrammVersion;
            StaLabelStatus.Text = "Status: waiting for action.";
        }

        // Pressing Enter/Return in the Call Box sets focus to the PW Box
        private void Call_PressedEnter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                TxtMyPW.Focus();
        }

        // Pressing Enter/Return in the password box will start the detailed search
        private void PW_PressedEnter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                CmdCallSearch_Click(sender, e);
        }


    }
}

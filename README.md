<H1>HamCallSearch</H1>

HamCallSearch is a free, open source application to get Call-Information of another OM.
Currently it only supports HamQTH.com.

![](https://weinmann.biz/images/hamcallsearch2100.png)

I am planning to include QRZ.com, but the API requires a subscriber plan which is not free. For free you can only get very basic information.
So for the moment I'm concentrating on the FREE services.


Future plans? Maybe I'll extend it with a web site crawler to find useful information anywhere in the web, not only in the Call Databases.


**Download the latest Binary for Windows here:**

https://gitlab.com/m0r0i/hamcallsearch/tree/master/Download/Windows

**Download the latest binary for Linux here:**

https://gitlab.com/m0r0i/hamcallsearch/tree/master/Download/Linux


<hr>
<h3>Credit for the icon:</h3>

Icons made by https://www.flaticon.com/authors/freepik from www.flaticon.com is licensed by http://creativecommons.org/licenses/by/3.0/
